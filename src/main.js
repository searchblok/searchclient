// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
require('es6-promise/auto')
import Vue from 'vue'
import Searchblok from './App'

/* eslint-disable no-new */
new Vue({
  el: 'searchblok',
  components: { Searchblok }
})
